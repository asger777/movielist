//
//  SingleMovieVC.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import UIKit
import XCDYouTubeKit

class SingleMovieVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var movieName: UILabel!
    @IBOutlet weak var watchTrailerBtn: UIButton!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    
    // Variables
    var movie: Movie?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK - Private Functions
    private func setupView() {
        setupTargets()
        
        if !movie!.poster_path.isEmpty {
            spinner.isHidden = false
            spinner.startAnimating()
            posterImageView.setPhotoWithURL(path: movie!.poster_path) { (image) in
                if image != nil {
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                }
            }
        }
        movieName.text = movie!.title
        genresLabel.text = movie!.genres.joined(separator: ", ")
        dateLabel.text = movie!.release_date
        overviewLabel.text = movie!.overview
    }
    private func setupTargets() {
        watchTrailerBtn.addTarget(self, action: #selector(watchTrailerBtnClicked), for: .touchUpInside)
    }
    
    
    // MARK - @objc Functions
    @objc func watchTrailerBtnClicked() {
        showLoading()
        AuthAPI.instance.getVideoKeyByID(movieID: movie!.id) { (videoID) in
            self.hideLoading()
            let videoVC = XCDYouTubeVideoPlayerViewController(videoIdentifier: videoID)
            DispatchQueue.main.async {
                self.present(videoVC, animated: true)
            }
        }
    }
}
