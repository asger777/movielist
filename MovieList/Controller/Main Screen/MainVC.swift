//
//  MainVC.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // Variables
    var topRefreshControl = UIRefreshControl()
    var movies = [Movie]()
    var pageNumber = 1
    
    var canLoadMore = true // for enabling of infinity scroll
    var isLoading = false // for infinity scroll
    var isSearching = false // this variable is for pagination of search query
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    // MARK - Private Functions
    private func setupView() {
        configureRefreshControl()
        getDatas()
        hideKeyboardWhenTappedAround()
    }
    private func configureRefreshControl() {
        topRefreshControl.backgroundColor = .clear
        topRefreshControl.addTarget(self, action: #selector(handleRefreshControl), for: .valueChanged)
        tableView.addSubview(topRefreshControl)
    }
    func getDatas() {
        if !topRefreshControl.isRefreshing && !isSearching {showLoading()}
        if isSearching && searchBar.text!.isEmpty {
            // if pagination page number changed but after that, user cleared search text
            // we should clear page number, for start again
            pageNumber = 1
        }
        if isSearching && !searchBar.text!.isEmpty {
            AuthAPI.instance.searchMovies(pageNumber: pageNumber, query: searchBar.text!) { (list, canLoadMore) in
                self.hideLoading()
                self.movies = list
                self.canLoadMore = canLoadMore
                DispatchQueue.main.async {
                    self.topRefreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        } else {
            AuthAPI.instance.getPopularList(pageNumber: pageNumber) { (list, canLoadMore) in
                self.hideLoading()
                self.movies = list
                self.canLoadMore = canLoadMore
                DispatchQueue.main.async {
                    self.topRefreshControl.endRefreshing()
                    self.tableView.reloadData()
                }
            }
        }
    }
    func getMoreDatas() {
        isLoading = true
        if isSearching {
            AuthAPI.instance.searchMovies(pageNumber: pageNumber, query: searchBar.text!) { (list, canLoadMore) in
                self.isLoading = false
                self.canLoadMore = canLoadMore
                self.movies.append(contentsOf: list)
                DispatchQueue.main.async {
                    if !self.topRefreshControl.isRefreshing {
                        self.topRefreshControl.endRefreshing()
                    }
                    self.tableView.reloadData()
                }
            }
        } else {
            AuthAPI.instance.getPopularList(pageNumber: pageNumber) { (list, canLoadMore) in
                self.isLoading = false
                self.canLoadMore = canLoadMore
                self.movies.append(contentsOf: list)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    func configureSearchBar(isHidden: Bool) {
        searchBar.showsCancelButton = !isHidden
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK - @objc functions
    @objc func handleRefreshControl() {
        pageNumber = 1
        isSearching = false
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.getDatas()
        }
    }
}
