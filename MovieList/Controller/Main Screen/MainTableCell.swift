//
//  MainTableCell.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import UIKit

class MainTableCell: UITableViewCell {
    
    // Outlets
    @IBOutlet weak var postImageView: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        postImageView.image = UIImage(named: "poster-placeholder")!
        movieNameLabel.text = nil
        spinner.isHidden = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func configureCell(item: Movie) {
        if !item.poster_path.isEmpty {
            spinner.isHidden = false
            spinner.startAnimating()
            postImageView.setPhotoWithURL(path: item.poster_path) { (image) in
                if image != nil {
                    self.spinner.stopAnimating()
                    self.spinner.isHidden = true
                }
            }
        }
        movieNameLabel.text = item.title
    }

}
