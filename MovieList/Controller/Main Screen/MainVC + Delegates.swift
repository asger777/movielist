//
//  MainVC + Delegates.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import UIKit

extension MainVC: UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UISearchBarDelegate {
    
    // MARK - TableView Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell") as! MainTableCell
        cell.configureCell(item: movies[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(!AuthAPI.instance.hasInternet()) {
            showAlert(type: .error, position: .center, body: "No Internet Connection")
            return
        }
        showLoading()
        AuthAPI.instance.getMovieById(movieID: movies[indexPath.row].id) { (movie) in
            self.hideLoading()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "SingleMovieVC") as! SingleMovieVC
            vc.movie = movie
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    // MARK - ScrollView Delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height * 4 {
            if !isLoading && canLoadMore {
                pageNumber += 1
                getMoreDatas()
            }
        }
    }
    // MARK - SearchBar Delegates
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearching = true
        configureSearchBar(isHidden: false)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        configureSearchBar(isHidden: true)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let query = searchText
        if !AuthAPI.instance.hasInternet() {
            movies = movies.filter({$0.title.contains(query)})
            tableView.reloadData()
        } else {
            getDatas()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        configureSearchBar(isHidden: true)
        searchBar.endEditing(true)
    }
}
