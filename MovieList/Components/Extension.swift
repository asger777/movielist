//
//  Extension.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import UIKit
import ChameleonFramework
import SwiftMessages
import NVActivityIndicatorView
import Kingfisher


extension UIImageView {
    func setPhotoWithURL(path: String, onComplete: ((_ image: UIImage?) -> ())? = nil ) {
        let baseUrl = AuthAPI.instance.IMAGE_BASE_URL
        if let photoUrl = URL(string: "\(baseUrl)\(path)") {
            let cacheKey = "\(photoUrl)"
            let cache = ImageCache.default
            if cache.isCached(forKey: cacheKey) {
                cache.retrieveImage(forKey: cacheKey) { (result) in
                    switch result {
                    case .success(let value):
                        DispatchQueue.main.async {
                            if let image = value.image {
                                self.image = image
                                onComplete?(self.image)
                            }
                        }
                        break
                    case .failure( _):
                        print("can not get from cache")
                        break
                    }
                }
            } else {
                ImageDownloader.default.downloadImage(with: photoUrl, options: [.targetCache(ImageCache(name: baseUrl))], progressBlock: nil, completionHandler: { (result) in
                    switch result {
                    case .success(let value):
                        DispatchQueue.main.async {
                            self.image = value.image
                            onComplete?(self.image)
                        }
                        cache.store(value.image, forKey: cacheKey)
                        break
                    case .failure( _):
                        break
                    }
                })
            }
            
        }
    }
}

extension UIViewController {
    @objc func goBackSimple() {
        navigationController?.popViewController(animated: true)
    }
    func setupModal() {
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
    }
    func showAlert(type: Theme = .error, position: SwiftMessages.PresentationStyle = .top, body: String) {
        let alertID = "notificationAlert"
        SwiftMessages.hide(id: alertID)
        let errorMessage = MessageView.viewFromNib(layout: .cardView)
        errorMessage.configureTheme(type)
        errorMessage.id = alertID
        errorMessage.button?.isHidden = true
        errorMessage.configureContent(body: body)
        errorMessage.titleLabel?.isHidden = true
        
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = position
        DispatchQueue.main.async {
            self.hideLoading()
            SwiftMessages.show(config: config, view: errorMessage)
        }
    }
    
    func hideLoading() {
        if let v = self.view.viewWithTag(1121) {
            UIView.animate(withDuration: 1.0, animations: {
                v.alpha = 0.0
            }) { (completed) in
                v.removeFromSuperview()
            }
        }
    }
    func showLoading() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4)
        view.tag = 1121
        let frame = CGRect(x: view.frame.width / 2 - 25, y: view.frame.height / 2 - 25, width: 50, height: 50)
        let indicator = NVActivityIndicatorView(frame: frame, type: .circleStrokeSpin,
                                                color: UIColor(hexString: "FFFFFF")!,
                                                padding: CGFloat(0))
        indicator.startAnimating()
        view.addSubview(indicator)
        
        UIView.transition(with: self.view, duration: 0.15, options: .transitionCrossDissolve, animations: {
            self.view.addSubview(view)
        }, completion: nil)
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func removeWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}
