//
//  AuthAPI.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import Reachability

struct AuthKeys {
    static let movies = "movies"
}

class AuthAPI {
    let YOUTUBE_API_KEY = "AIzaSyA6ADLxlPsx6IfyF6dLQ3BOgU70G3RJbHY"
    let API_KEY = "aa53aff8f64a198da133e45fcaba1d66"
    let BASE_URL = "https://api.themoviedb.org/3"
    let IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w342"
    lazy var MOVIE_URL = "\(BASE_URL)/movie"
    
    // Lists Urls
    lazy var POPULAR_LIST_URL = "\(MOVIE_URL)/popular"
    lazy var SEARCH_MOVIE_URL = "\(BASE_URL)/search/movie"
    
    
    static let instance = AuthAPI()
    var userDefaults: UserDefaults
    var reach = Reachability()!
    var HEADERS = [
        "Content-Type": "application/json"
    ]
    
    private init() {
        userDefaults = UserDefaults.standard
    }
    
    func setMovies (_ movies: [Movie]) {
        let encodedMovies = NSKeyedArchiver.archivedData(withRootObject: movies)
        userDefaults.setValue(encodedMovies, forKey: AuthKeys.movies)
        userDefaults.synchronize()
    }
    func getMovies() -> [Movie] {
        if let data = userDefaults.data(forKey: AuthKeys.movies), let movies = NSKeyedUnarchiver.unarchiveObject(with: data) as? [Movie] {
            return movies
        }
        
        return [Movie]()
    }
    
    func getPopularList(pageNumber: Int, onComplete: @escaping (_ list: [Movie], _ canLoadMore: Bool) -> ()) {
        var urlComps = URLComponents(string: POPULAR_LIST_URL)!
        let queryItems = [
            URLQueryItem(name: "api_key", value: API_KEY),
            URLQueryItem(name: "page", value: "\(pageNumber)")
        ]
        urlComps.queryItems = queryItems
        print("url", urlComps.url!)
        if (!hasInternet()) {
            // if there is no internet, return list from userdefaults
            onComplete(getMovies(), false)
        }
        Alamofire.request("\(urlComps.url!)", method: .get, encoding: JSONEncoding.default, headers: HEADERS).responseJSON { [unowned self] (response) in
            let json = JSON(response.value as Any)
            var list = [Movie]()
            for movie in json["results"].arrayValue {
                let model = Movie(json: movie)
                list.append(model)
            }
            self.setMovies(list)
            onComplete(list, pageNumber < json["total_pages"].intValue)
        }
    }
    func searchMovies(pageNumber: Int, query: String, onComplete: @escaping (_ list: [Movie], _ canLoadMore: Bool) -> ()) {
        var urlComps = URLComponents(string: SEARCH_MOVIE_URL)!
        let queryItems = [
            URLQueryItem(name: "api_key", value: API_KEY),
            URLQueryItem(name: "page", value: "\(pageNumber)"),
            URLQueryItem(name: "query", value: query),
        ]
        urlComps.queryItems = queryItems
        print("url", urlComps.url!)
        Alamofire.request("\(urlComps.url!)", method: .get, encoding: JSONEncoding.default, headers: HEADERS).responseJSON { [unowned self] (response) in
            let json = JSON(response.value as Any)
            var list = [Movie]()
            for movie in json["results"].arrayValue {
                let model = Movie(json: movie)
                list.append(model)
            }
            self.setMovies(list)
            onComplete(list, pageNumber < json["total_pages"].intValue)
        }
    }
    func getMovieById(movieID: Int, onComplete: @escaping (_ movie: Movie) -> ()) {
        var urlComps = URLComponents(string: "\(MOVIE_URL)/\(movieID)")!
        let queryItems = [
            URLQueryItem(name: "api_key", value: API_KEY)
        ]
        urlComps.queryItems = queryItems
        print("url", urlComps.url!)
        Alamofire.request("\(urlComps.url!)", method: .get, encoding: JSONEncoding.default, headers: HEADERS).responseJSON { (response) in
            let json = JSON(response.value as Any)
            onComplete(Movie(json: json))
        }
    }
    func getVideoKeyByID(movieID: Int, onComplete: @escaping (_ key: String) -> ()) {
        var urlComps = URLComponents(string: "\(MOVIE_URL)/\(movieID)/videos")!
        let queryItems = [
            URLQueryItem(name: "api_key", value: API_KEY)
        ]
        urlComps.queryItems = queryItems
        print("url", urlComps.url!)
        Alamofire.request("\(urlComps.url!)", method: .get, encoding: JSONEncoding.default, headers: HEADERS).responseJSON { (response) in
            let json = JSON(response.value as Any)
            let results = json["results"].arrayValue
            onComplete(results[0]["key"].stringValue) // just get first element of array and get it
        }
    }
    
    func hasInternet() -> Bool {
        return reach.connection != .none
    }
}
