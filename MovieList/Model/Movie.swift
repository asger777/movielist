//
//  Movie.swift
//  MovieList
//
//  Created by Asker Amrahov on 4/5/19.
//  Copyright © 2019 Amrahov. All rights reserved.
//

import Foundation
import SwiftyJSON

class Movie: NSObject, NSCoding {
    var id: Int = 0,
    title: String = "",
    poster_path: String = "",
    genres: [String] = [String](),
    overview: String = "",
    release_date: String = ""
    
    init(json: JSON) {
        id = json["id"].intValue
        title = json["title"].stringValue
        poster_path = json["poster_path"].stringValue
        for genre in json["genres"].arrayValue {
            genres.append(genre["name"].stringValue)
        }
        overview = json["overview"].stringValue
        release_date = json["release_date"].stringValue
    }
    required init(coder decoder: NSCoder) {
        id = decoder.decodeInteger(forKey: "id")
        title = decoder.decodeObject(forKey: "title") as? String ?? ""
        poster_path = decoder.decodeObject(forKey: "poster_path") as? String ?? ""
        genres = decoder.decodeObject(forKey: "genres") as? [String] ?? [String]()
        overview = decoder.decodeObject(forKey: "overview") as? String ?? ""
        release_date = decoder.decodeObject(forKey: "release_date") as? String ?? ""
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(id, forKey: "id")
        coder.encode(title, forKey: "title")
        coder.encode(poster_path, forKey: "poster_path")
        coder.encode(genres, forKey: "genres")
        coder.encode(overview, forKey: "overview")
        coder.encode(release_date, forKey: "release_date")
    }
}
